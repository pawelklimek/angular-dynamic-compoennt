import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {StickyNoteService} from './dynamic-components/sticky-note/sticky-note.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  @ViewChild(
    'StickyNoteTemplate',
    {
      read: ViewContainerRef
    }
  ) stickyNoteTemplate: ViewContainerRef;


  constructor(private stickyNoteService: StickyNoteService) {
  }

  ngOnInit(): void {
    this.listenOpenStickyNotes();
  }

  private listenOpenStickyNotes(): void {
    this.stickyNoteService.open(() => this.stickyNoteTemplate);
  }
}
